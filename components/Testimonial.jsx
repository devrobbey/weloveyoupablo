import styles from '../styles/Testimonial.module.css'
import {useInView} from 'react-intersection-observer';

function ImageAndName(props) {
    return (
        <div>
            <div style={{
                width: '10rem',
                display: 'flex',
                flexDirection: 'row',
                flexWrap: 'nowrap',
            }}>
                <img src={props.src} className={styles.personImage} />
                <div className={styles.textNextToPic}>
                    <div className={styles.name}>
                        <span>{props.name}</span>
                    </div>
                    <div className={styles.personDescription}>
                        <span>{props.personDescription}</span>
                    </div>
                </div>
            </div>
        </div>);
}

export default function Testimonial(props) {

    const [ref, inView] = useInView({threshold: 0});

    return (<>
        <div className={[styles.container, inView ? styles.show : styles.hidden].join(' ')}>
            <div 
            ref={ref} 
            style={{
                justifyContent: 'space-evenly',
                display: 'flex',
                flexDirection: 'row',
                flexWrap: 'nowrap',
                margin: 'auto',
            }}>
                <ImageAndName src={props.src} name={props.name} personDescription={props.personDescription} />

            </div>
            <div className={styles.testimonial}>{props.children}</div>
        </div>
    </>);
}
