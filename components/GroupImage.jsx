import React from "react"


const GroupImage = (props) => {
    return <div className="flex justify-center" >
        <img className="max-h-screen"
            src={props.src}
        />
    </div>
}

export default GroupImage;