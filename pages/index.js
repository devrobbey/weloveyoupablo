import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Testimonial from '../components/Testimonial'
import GroupImage from '../components/GroupImage'

export default function Home() {

  return (
    <>
      <Head>
        <title>WE LOVE YOU PABLO!</title>
        <meta name="description" content="We love you pablo - official love spreading place!" />
        <link rel="icon" href="/favicon.ico" />
      </Head>


      <div className={styles.backgroundColor} >
        <main className={styles.main}>
          <img
            src={'/images/hero.png'}
            style={{
              display: 'flex',
              height: '50%',
              width: '100%',
              backgroundPosition: 'center',
              backgroundRepeat: 'no-repeat',
              backgroundSize: 'cover',
              position: 'relative'
            }}
          />
          <div className={styles.testimonialsList}>

            {/* <h1 className={styles.title}>
              See how much love people have for Pablo!
            </h1> */}
            <Testimonial
              src='/images/chupito.png'
              name='Chupito'>
              <p>Amaaazing pabliño, amazing was the time we shared and the memories we created. Thank you for helping me both personally and professionally, you are an incredible person, we will see you soon my brother</p>
            </Testimonial>
            <Testimonial
              src='/images/sammy.jpeg'
              name='Sammy'>
              <p>Pablo is an AMAAAAAAZING human being. He&apos;s a friend to all and is always willing to lend a helping hand and share his treats ;) I&apos;m grateful that the universe made our paths cross, he&apos;s one of the most positive people i&apos;ve ever met and I wish him nothing but love, luck and success ❤ 100/10 </p>
            </Testimonial>
          </div>

          <GroupImage src="/images/group1.png" />
          <div className={styles.testimonialsList}>
            <Testimonial
              src='/images/seba.png'
              name='Seba'>
              <p>Pablito!!!  Thank you for sharing your good energy every day and teaching with your actions to be a better person.  Meeting people like you makes traveling better.  see you soon ;)</p>
            </Testimonial>
            <Testimonial
              src='/images/shakira.png'
              name='Cris'>
              <p>Some people don&apos;t like french. Some french don&apos;t like french. But everybody likes Pablo. Cause he simply is a pure ray of sunshine.
                One that always makes you feel welcome with his smile and his famous (the only one he bakes) pie. I will truly miss you, gonna be hard to find a match for my dance moves, someone that spreads joy like you. Love you, Pablito.</p>
            </Testimonial>
          </div>

          <GroupImage src="/images/group2.png" />

          <div className={styles.testimonialsList}>
            <Testimonial
              src='/images/shaiv.png'
              name='Shaiv'>
              <p>Pablo is such an amazing guy who has a talent for making friends with everyone and making them feel welcome. I&apos;ve never met someone as generous as him and he easily made my time at circles 10x better</p>
            </Testimonial>
            <Testimonial
              src='/images/sandra.png'
              name='Sandra'>
              <p>Hello Pablo! I feel very grateful for having had the opportunity to meet you and to have shared great moments in Barcelona. Days went by so fast! Beautiful memories remain. Thanks for everything. I remember you cooking, dancing, laughing, giving good advice and your special greetings, always. My best wishes and hopefully we will see each other again.</p>
            </Testimonial>
          </div>

          <GroupImage src="/images/group3.png" />

          <div className={styles.testimonialsList}>
            <Testimonial
              src='/images/alona.jpeg'
              name='Alona'>
              <p>The one who lights up the room with the smile. Moves like a pro latino dancer, even though he is not. Sings amazingly and bakes the BEST cakes and cookies (especially those with bananas!) to bring up people together and share the moments of joy. Makes all that with open heart and love.. Truly gratefull you&apos;ve been a part of my Circles journey. We love you!</p>
            </Testimonial>
            <Testimonial
              src='/images/lucien.png'
              name='Lucien'
              personDescription=' '>
              <p>Pablo a such good friend with a such beautiful souls I enjoy all the moment we pass together from our karaoke to our deep conversation your are just what a ideal friend should be</p>
            </Testimonial>
          </div>

          <GroupImage src="/images/group4.png" />

          <div className={styles.testimonialsList}>
            <Testimonial
              src='/images/nat.png'
              name='Natalia'
              personDescription=' '>
              <p>I just wanted to tell you how happy we&apos;ve been having you at Circles, you are an example of the kind of peolple we want to have! You&apos;ve been an inspiration to us seeing how members can be so self reliance, responsible and always in a good mood! People as you make us feel so proud of this project we&apos;ve built.  You&apos;ll always be welcomed! Safe travels and good luck 😊</p>
            </Testimonial>
            <Testimonial
              src='/images/robbie.png'
              name='Robbie'
              personDescription=' '>
              <p>I freakin love Pablo! Not only he brings joy to the ones around him. He also spreads love and at every moment gives you the feeling of being welcome. He made the time living with him to the most valuable in my life. I am very grateful for knowing Pablo!</p>
            </Testimonial>
          </div>
          <div className='flex'>
            <img
              src={'/images/circlesPano.png'}
            />
          </div>
          <footer style={{
            textAlign: 'center',
            padding: '10px',
            margin: '0px',
            color: 'black',
            bottom: 0,
            height: '2.5rem',
            //because we get horizontal scrollbar otherwise.
            width: '98.6%'
          }}>
            Sponsored by the ❤ for Pablo | created by{' '}
            <a href='https://spoz.app'>
              Robbie
            </a>
          </footer>
        </main>
      </div>
    </>
  )
}
